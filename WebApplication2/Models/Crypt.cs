﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Crypt
    {
        
        public string EncryptMessage { get; set; }
        public string keyed_alphabet { get; set; }
        public string CryptLine()
        {
             return  $"message: \"" + this.EncryptMessage +"  \", keyed_alphabet: \""+ this.keyed_alphabet +"\"";
        }
    }
}
