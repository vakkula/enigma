using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication2.Controllers
{
    public class EnCryptController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();

        }

        [HttpPost]
        public IActionResult Index(Models.EnCrypt ec)
        {
            ViewData["Message"] = ec.Message;
           

            Alphabet al = new Alphabet();
            List<char> mas = new List<char>(al.CompAlph.ToCharArray());
            // List<char> input = new List<char>(Console.ReadLine().ToUpper());
            List<char> input = new List<char>(ec.Message.ToUpper());
            MakeMixList(mas);
            List<char> CryptoMas = new List<char>(mas);



            List<string> CryptoMessage = new List<string>();
            string crm = "";
            for (int j = 0; j < input.Count; j++)
            {
                char index = input[j];
                if (index == ' ')
                {
                    crm = crm + " ";
                }
                else
                {
                    int inx = al.CompAlph.IndexOf(index);
                    char index2 = CryptoMas[inx];
                    int CrMes = al.CompAlph.IndexOf(index2);
                    if (CrMes < 10)
                    {
                        CryptoMessage.Add("0" + CrMes);
                        crm = crm + ("0" + CrMes);
                    }
                    else
                    {
                        CryptoMessage.Add(CrMes.ToString());
                        crm = crm + CrMes.ToString();
                    }
                }
            }
            ViewData["MessageComp"] = crm;
            ViewData["Key_alphabet"] = string.Join("",CryptoMas);
            return View();
        }



       

        public static void MakeMixList<T>(IList<T> list)
            {
                Random r = new Random();

                SortedList<int, T> mixedList = new SortedList<int, T>();
                foreach (T item in list)
                    mixedList.Add(r.Next(), item);

                list.Clear();
                for (int i = 0; i < mixedList.Count; i++)
                {
                    list.Add(mixedList.Values[i]);
                }
                //mixedList.Clear(); 
                
            }
       


    }
}