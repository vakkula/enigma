using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class CryptController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
           // string line = "message: \"012222 1114142503 0313012513 03141418192102 0113 2419182119021713 06131715070119\", keyed_alphabet: \"BHISOECRTMGWYVALUZDNFJKPQX\"";
            
            return View();
        }
        [HttpPost]
        public IActionResult Index(Models.Crypt mc)
        {
            ViewData["Message"] = mc.CryptLine();
            ViewData["MessageComp"] = Cryptos.Result(mc.CryptLine());
            return View();
        }

    }

    public class Cryptos
    {
        public static string Result(string value)
        {
            var Mass = value.Split(',');
            var message2 = Convert.ToString(Mass[0]).Substring(10);
            var message = Convert.ToString(Mass[0]).Remove(0, 10).TrimEnd('\"');
            string key = Convert.ToString(Mass[1]).Remove(0, 18).TrimEnd('\"');
            Alphabet a = new Alphabet();
            List<string> str = new List<string>();
            try
            {
                for (int i = 0; i < message.Length; i++)
                {
                    char numm = message[i];
                    char numm2 = message[i + 1];
                    if (message[i] == ' ')
                    {
                        str.Add(" ");
                    }

                    else
                    {
                        string res = String.Concat((int)Char.GetNumericValue(numm), (int)Char.GetNumericValue(numm2));
                        int indx = Int32.Parse(res);
                        char c = Convert.ToChar(a.CompAlph.Substring(indx, 1));
                        int inx = key.IndexOf(c);
                        str.Add(a.CompAlph.Substring(inx, 1));
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            string rres ="";
            foreach (var sti in str)
            {
                rres += sti; 
            }
            return rres;
        }
    }
    

    public class Alphabet
    {
        static string alph = "abcdefghijklmnopqrstuvwxyz";
        public string CompAlph = Convert.ToString(alph).ToUpper();
    }
}